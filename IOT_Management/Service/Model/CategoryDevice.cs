﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Service.Model
{
    public class CategoryDevice
    {
        public int Id { set; get; }
        public string Name { set; get; }
        [AllowHtml]
        public string Description { set; get; }
        public string ImageUrl { set; get; }
        public ICollection<Device> Device { set; get; }
    }
}