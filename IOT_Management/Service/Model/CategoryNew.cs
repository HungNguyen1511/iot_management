﻿using System.Collections.Generic;

namespace Service.Model
{
    public class CategoryNew
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Desciption { set; get; }
        public string ImageUrl { set; get; }
        public ICollection<New> News { set; get; }
        public string ImageName { set; get; }
    }
}