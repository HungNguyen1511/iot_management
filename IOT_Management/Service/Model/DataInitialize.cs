﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Service.Model
{
    public class DataInitialize : DropCreateDatabaseIfModelChanges<ManagementLabContext>
    {
        protected override void Seed(ManagementLabContext context)
        {

            InitializeIdentityForEF(context);
            base.Seed(context);
            List<CategoryDevice> categorieDevies = new List<CategoryDevice>()
            {
                new CategoryDevice()
                {
                    Name = "Bàn",
                    Description = "Table",  
                    
                },
                new CategoryDevice()
                {
                    Name = "Bộ dụng cụ",
                    Description = "Toolbox",
                }

            };

            List<Device> devices = new List<Device>()
            {
                new Device()
                {
                    CategoryDeviceId = 1,
                    Name = "Bàn cảm biến quang loại",
                    CompanyName = "abc",
                    Modified = DateTime.Now,
                    Date_Import = DateTime.Now,
                    Description = "",
                    TotalNumber = 10,
                    TotalRest = 10,
                    
                },
                new Device()
                {
                    CategoryDeviceId = 2,
                    Name = "Bộ dụng cụ mạch điện",
                    CompanyName = "abc",
                    Modified = DateTime.Now,
                    Date_Import = DateTime.Now,
                    Description = "",
                    TotalNumber = 10,
                    TotalRest = 10,
                },
            };

            List<CategoryNew> categoryNews = new List<CategoryNew>()
            {
                new CategoryNew()
                {
                    Name = "Tin tức",
                    Desciption = "Tổng hợp tin tức mới nhất",     
                    
                }
            };

            List<New> news = new List<New>()
            {
                new New()
                {
                    Name = "Thông báo",
                    CategoryNewId = 1,
                    Post_Content = "abgiaebguiabefuiabfuo",
                    Post_On = DateTime.Now,
                    Short_Desciption = "agwhowkrhpkaemrhpmehr0",
                    UrlSlug = "thong-bao",
                    RateCount = 0,
                    Title = "Thông báo",
                    TotalRate = 0,            
                    Modified = DateTime.Now,
                    Published = true,
                    ViewCount = 0
                }
            };

            List<Tag> tags = new List<Tag>()
            {
                new Tag()
                {
                    
                }
            };

            context.CategoryDevices.AddRange(categorieDevies);
            context.Devices.AddRange(devices);
            context.Tags.AddRange(tags);
            context.News.AddRange(news);
            context.CategoryNews.AddRange(categoryNews);
            context.SaveChanges();
        }

        public static void InitializeIdentityForEF(ManagementLabContext db)
        {
            var userManager = new UserManager<UserFactor>(new UserStore<UserFactor>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            const string name = "admin@example.com";
            const string password = "Admin@123456";
            const string roleName = "Admin";

            //Create Role Admin if it does not exist
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.Create(role);
            }

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new UserFactor { UserName = name, Email = name };
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }

    }
}