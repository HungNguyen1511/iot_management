﻿using System;

namespace Service.Model
{
    public class TimelineEvent
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Desciption { set; get; }
        public DateTime Start_date { set; get; }
        public DateTime End_Date { set; get; }
    }
}