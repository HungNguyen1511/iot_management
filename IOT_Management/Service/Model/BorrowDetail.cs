﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Service.Model
{
    public class BorrowDetail
    {
        public int Id { set; get; }

        [ForeignKey("Device")]
        public int DeviceId { set; get; }

        [ForeignKey("UserFactor")]
        public string UserId { set; get; }
        public int TotalBorrow { set; get; }
        [UIHint("IsReturn")]
        public bool IsReturn { set; get; }
        public DateTime Start_Date { set; get; }
        public DateTime? End_Date { set; get; }

        public virtual Device Device { get; set; }
        public virtual UserFactor UserFactor { get; set; }
    }
}