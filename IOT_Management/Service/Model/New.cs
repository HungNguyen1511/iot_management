﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Service.Model
{
    public class New
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int PostId { set; get; }

        [Required(ErrorMessage = "Title name is required.")]
        [StringLength(255)]
        public string Title { set; get; }

        [StringLength(255)]
        [AllowHtml]
        public string Short_Desciption { set; get; }
        [AllowHtml]
        public string Post_Content { set; get; }

        [StringLength(255)]
        public string UrlSlug { set; get; }

        public bool Published { set; get; }
        public int ViewCount { set; get; }
        public int RateCount { set; get; }
        public int TotalRate { set; get; }
        public decimal Rate { get { return TotalRate / RateCount; } }
        public DateTime Post_On { set; get; }
        public DateTime Modified { set; get; }

        [ForeignKey("CategoryNew")]
        public int CategoryNewId { get; set; }

        public virtual CategoryNew CategoryNew { set; get; }
        public virtual ICollection<Tag> Tags { get; set; }
        public string ImageUrl { set; get; }

        public string ImageName { set; get; }
    }
}