﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Service.Model
{
    public class Device
    {
        public int Id { set; get; }

        [Required(ErrorMessage = "Title name is required.")]
        public string Name { set; get; }
        [AllowHtml]
        public string Description { set; get; }
        public int TotalNumber { set; get; }
        public int TotalRest { get; set; }
        public string CompanyName { set; get; }
        public DateTime Date_Import { set; get; }
        public DateTime? Modified { set; get; }
        public string ImageUrl { set; get; }
        public string ImageName { set; get; }

        [ForeignKey("CategoryDevice")]
        public int CategoryDeviceId { set; get; }

        public CategoryDevice CategoryDevice { set; get; }

        public virtual IList<OutMemberBorrow> OutMemberBorrow { get; set; }
    }
}