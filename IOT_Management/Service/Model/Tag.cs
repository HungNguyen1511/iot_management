﻿using System.Collections.Generic;

namespace Service.Model
{
    public class Tag
    {
        public int TagId { set; get; }
        public string Name { set; get; }
        public string UrlName { set; get; }
        public string Description { set; get; }
        public int Count { set; get; }
        public virtual ICollection<New> News { set; get; }
    }
}