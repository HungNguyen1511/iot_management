﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Service.Model
{
    public class UserFactor : IdentityUser
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Password { set; get; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserFactor> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}