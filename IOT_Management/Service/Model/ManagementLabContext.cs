﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Service.Model
{
    public class ManagementLabContext : IdentityDbContext<UserFactor>
    {
        public ManagementLabContext() : base("name=ManagementLabContext", throwIfV1Schema: false)
        {
            //Database.SetInitializer<ManagementLabContext>(new DataInitialize());
        }

        static ManagementLabContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ManagementLabContext>(new DataInitialize());
        }

        public static ManagementLabContext Create()
        {
            return new ManagementLabContext();
        }

        public DbSet<New> News { get; set; }
        public DbSet<CategoryDevice> CategoryDevices { get; set; }
        public DbSet<CategoryNew> CategoryNews { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TimelineEvent> TimelineEvents { get; set; }
        public DbSet<BorrowDetail> BorrowDetails { get; set; }
        public DbSet<OutMemberBorrow> OutMemberBorrows { get; set; }
    }
}