﻿using Service.Model;
using Service.Service.Interface;

namespace Service.Service.Repository
{
    public class DeviceRepository : RepositoryBase<Device>, IDeviceRepository
    {
    }
}