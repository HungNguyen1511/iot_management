﻿using Service.Model;
using Service.Service.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Service
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly ManagementLabContext _db;
        private readonly DbSet<T> dbSet;

        public RepositoryBase()
        {
            _db = new ManagementLabContext();
            this.dbSet = _db.Set<T>();
        }

        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual T Find(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.AsEnumerable();
        }

        public virtual void Update(T entity)
        {
            dbSet.Attach(entity);
            _db.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Commit()
        {
            _db.SaveChanges();
        }
    }
}