﻿using Service.Model;
using Service.Service.Interface;

namespace Service.Service.Repository
{
    public class BorrowDetailRepository : RepositoryBase<BorrowDetail>, IBorrowDetailRepository
    {
    }
}