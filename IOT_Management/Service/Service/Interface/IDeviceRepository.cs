﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface IDeviceRepository : IRepositoryBase<Device>
    {
    }
}