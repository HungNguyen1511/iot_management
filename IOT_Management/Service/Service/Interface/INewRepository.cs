﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface INewRepository : IRepositoryBase<New>
    {
    }
}