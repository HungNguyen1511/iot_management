﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface ITimelineEventRepository : IRepositoryBase<TimelineEvent>
    {
    }
}