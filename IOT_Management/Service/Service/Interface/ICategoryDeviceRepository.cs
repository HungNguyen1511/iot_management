﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface ICategoryDeviceRepository : IRepositoryBase<CategoryDevice>
    {
    }
}