﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface IUserRepository : IRepositoryBase<UserFactor>
    {
    }
}