﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
    }
}