﻿using System.Collections.Generic;

namespace Service.Service.Interface
{
    public interface IRepositoryBase<T>
    {
        T Find(int id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        IEnumerable<T> GetAll();

        void Commit();
    }
}