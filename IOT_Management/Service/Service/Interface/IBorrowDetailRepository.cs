﻿using Service.Model;

namespace Service.Service.Interface
{
    public interface IBorrowDetailRepository : IRepositoryBase<BorrowDetail>
    {
    }
}