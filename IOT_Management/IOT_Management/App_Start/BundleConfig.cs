﻿using System.Web.Optimization;

namespace IOT_Management
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            bundles.Add(new StyleBundle("~/bundles/admincss").Include(
                      "~/Areas/Admin/Assets/vendor/fontawesome-free/css/all.min.css",
                      "~/Areas/Admin/Assets/css/sb-admin-2.min.css",
                      "~/Assets/fulldaycalendar/main.css",
                      "~/Areas/Admin/Assets/css/bootstrap-datetimepicker.min.css",
                      "~/Areas/Admin/Assets/css/style.css"));

            bundles.Add(new ScriptBundle("~/bundles/adminjs").Include(
                        "~/Scripts/ckeditor/ckeditor.js",
                        "~/Areas/Admin/Assets/vendor/jquery/jquery.min.js",
                        "~/Areas/Admin/Assets/vendor/bootstrap/js/bootstrap.bundle.min.js",
                        "~/Areas/Admin/Assets/vendor/jquery-easing/jquery.easing.min.js",
                        "~/Areas/Admin/Assets/js/sb-admin-2.min.js",
                        "~/Areas/Admin/Assets/js/moment.js",
                        "~/Areas/Admin/Assets/js/bootstrap-datetimepicker.min.js",
                      "~/Assets/fulldaycalendar/main.js",
                      "~/Areas/Admin/Assets/js/admin.js"));
        }
    }
}