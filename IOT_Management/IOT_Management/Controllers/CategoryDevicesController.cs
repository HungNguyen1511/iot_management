﻿using Service.Model;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IOT_Management.Controllers
{
    [Authorize]
    public class CategoryDevicesController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: CategoryDevices
        public ActionResult Index()
        {
            return View(db.CategoryDevices.ToList());
        }

        public ActionResult GetAll()
        {
            var all = db.CategoryDevices.ToList();
            return PartialView("_CategoryDeviceMenu", all);
        }

        // GET: CategoryDevices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // GET: CategoryDevices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoryDevices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,ImageUrl")] CategoryDevice categoryDevice)
        {
            if (ModelState.IsValid)
            {
                db.CategoryDevices.Add(categoryDevice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoryDevice);
        }

        // GET: CategoryDevices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // POST: CategoryDevices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,ImageUrl")] CategoryDevice categoryDevice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoryDevice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoryDevice);
        }

        // GET: CategoryDevices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // POST: CategoryDevices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            db.CategoryDevices.Remove(categoryDevice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}