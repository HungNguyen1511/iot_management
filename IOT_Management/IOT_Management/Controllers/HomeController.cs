﻿using System.Web.Mvc;

namespace IOT_Management.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoginRederect(string returnUrl)
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Devices", new { area = "Admin" });
            }
            else
                return RedirectToAction("Index", "Home", new { area = "" });
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}