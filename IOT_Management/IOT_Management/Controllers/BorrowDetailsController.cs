﻿using Microsoft.AspNet.Identity;
using Service.Model;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IOT_Management.Controllers
{
    [Authorize]
    public class BorrowDetailsController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: BorrowDetails
        [Authorize]
        [HttpPost]
        public JsonResult Borrow(int deviceId, int totalBorrow, string startDate, string endDate)
        {
            Device device = db.Devices.Find(deviceId);
            device.TotalRest = device.TotalRest - totalBorrow;
            if(device.TotalRest < 0)
            {
                device.TotalRest = 0;
            }
            BorrowDetail borrowDetail = new BorrowDetail()
            {
                Device = device,
                DeviceId = deviceId,
                Start_Date = DateTime.Parse(startDate),
                End_Date = DateTime.Parse(endDate),
                TotalBorrow = totalBorrow,
                UserId = User.Identity.GetUserId()
        };
            db.BorrowDetails.Add(borrowDetail);
          
            return Json(db.SaveChanges() > 0);
        }

        
    }
}