﻿using Service.Model;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryDevicesController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: Admin/CategoryDevices
        public ActionResult Index()
        {
            return View(db.CategoryDevices.ToList());
        }

        public ActionResult GetAll()
        {
            var category = db.CategoryDevices.ToList();
            return PartialView("_CategoryMenu", category);
        }


        // GET: Admin/CategoryDevices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // GET: Admin/CategoryDevices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/CategoryDevices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,ImageUrl")] CategoryDevice categoryDevice)
        {
            if (ModelState.IsValid)
            {
                db.CategoryDevices.Add(categoryDevice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoryDevice);
        }

        // GET: Admin/CategoryDevices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // POST: Admin/CategoryDevices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,ImageUrl")] CategoryDevice categoryDevice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoryDevice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoryDevice);
        }

        // GET: Admin/CategoryDevices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            if (categoryDevice == null)
            {
                return HttpNotFound();
            }
            return View(categoryDevice);
        }

        // POST: Admin/CategoryDevices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoryDevice categoryDevice = db.CategoryDevices.Find(id);
            db.CategoryDevices.Remove(categoryDevice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}