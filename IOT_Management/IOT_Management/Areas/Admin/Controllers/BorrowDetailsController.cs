﻿using OfficeOpenXml;
using OfficeOpenXml.Table;
using Service.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BorrowDetailsController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: Admin/BorrowDetails
        public ActionResult Index()
        {
            var borrowDetails = db.BorrowDetails.Include(b => b.Device).Include(b => b.UserFactor);
            return View(borrowDetails.ToList());
        }

        // GET: Admin/BorrowDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BorrowDetail borrowDetail = db.BorrowDetails.Find(id);
            if (borrowDetail == null)
            {
                return HttpNotFound();
            }
            return View(borrowDetail);
        }

        public ActionResult ReturnDevice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BorrowDetail borrowDetail = db.BorrowDetails.Find(id);
            if (borrowDetail == null)
            {
                return HttpNotFound();
            }
            borrowDetail.IsReturn = true;
            borrowDetail.End_Date = DateTime.Now;

            Device device = db.Devices.Find(borrowDetail.DeviceId);
            device.TotalRest += borrowDetail.TotalBorrow;
            if (device.TotalRest > device.TotalNumber)
            {
                device.TotalRest = device.TotalNumber;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void Export()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage(new FileInfo("MyWorkbook.xlsx")))
            {
                List<BorrowDetail> listBorrowDetail = db.BorrowDetails.ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Device", typeof(string));
                dt.Columns.Add("User", typeof(string));
                dt.Columns.Add("TotalBorrow", typeof(int));
                dt.Columns.Add("StartDate", typeof(string));
                dt.Columns.Add("EndDate", typeof(string));
                dt.Columns.Add("IsReturn", typeof(string));
                foreach (var item in listBorrowDetail)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = item.Id;
                    dr[1] = item.Device.Name;
                    dr[2] = item.UserFactor.UserName;
                    dr[3] = item.TotalBorrow;
                    dr[4] = item.Start_Date.ToString("dd/MM/yyyy");
                    dr[5] = item.End_Date.HasValue?item.End_Date.Value.ToString("dd/MM/yyyy"):null;
                    dr[6] = item.IsReturn.ToString();
                    dt.Rows.Add(dr);
                }
                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells[1, 1].LoadFromDataTable(dt, true, TableStyles.None);
                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=BorrowDetail.xlsx");
                    excel.SaveAs(memoryStream); memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}