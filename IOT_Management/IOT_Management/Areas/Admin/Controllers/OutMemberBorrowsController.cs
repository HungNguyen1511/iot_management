﻿using OfficeOpenXml;
using OfficeOpenXml.Table;
using Service.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OutMemberBorrowsController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();
        // GET: Admin/OutMemberBorrow
        public ActionResult Index()
        {
            var borrowDetail = db.OutMemberBorrows.Include("Device");
            return View(borrowDetail.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.DeviceId = new SelectList(db.Devices, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, StudentID, StudentName, Phone, Email, TotalBorrow, DeviceId")] OutMemberBorrow outMemberBorrow)
        {
            if (ModelState.IsValid)
            {
                Device device = db.Devices.Find(outMemberBorrow.DeviceId);
                device.TotalRest = device.TotalRest - outMemberBorrow.TotalBorrow;
                if (device.TotalRest < 0)
                {
                    device.TotalRest = 0;
                }

                outMemberBorrow.Device = device;
                outMemberBorrow.StartDate = DateTime.Now;
                db.OutMemberBorrows.Add(outMemberBorrow);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(outMemberBorrow);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OutMemberBorrow borrowDetail = db.OutMemberBorrows.Find(id);
            if (borrowDetail == null)
            {
                return HttpNotFound();
            }
            return View(borrowDetail);
        }

        public ActionResult ReturnDevice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OutMemberBorrow borrowDetail = db.OutMemberBorrows.Find(id);
            if (borrowDetail == null)
            {
                return HttpNotFound();
            }
            borrowDetail.IsReturn = true;
            borrowDetail.EndDate = DateTime.Now;

            Device device = db.Devices.Find(borrowDetail.DeviceId);
            device.TotalRest += borrowDetail.TotalBorrow;
            if (device.TotalRest > device.TotalNumber)
            {
                device.TotalRest = device.TotalNumber;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void Export()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage(new FileInfo("MyWorkbook.xlsx")))
            {
                List<OutMemberBorrow> listBorrowDetail = db.OutMemberBorrows.ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Device", typeof(string));
                dt.Columns.Add("UserID", typeof(string));
                dt.Columns.Add("UserName", typeof(string));
                dt.Columns.Add("TotalBorrow", typeof(int));
                dt.Columns.Add("StartDate", typeof(string));
                dt.Columns.Add("EndDate", typeof(string));
                dt.Columns.Add("IsReturn", typeof(string));
                foreach (var item in listBorrowDetail)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = item.Id;
                    dr[1] = item.Device.Name;
                    dr[2] = item.StudentID;
                    dr[3] = item.StudentName;
                    dr[4] = item.TotalBorrow;
                    dr[5] = item.StartDate.ToString("dd/MM/yyyy");
                    dr[6] = item.EndDate.HasValue?item.EndDate.Value.ToString("dd/MM/yyyy"):null;
                    dr[7] = item.IsReturn.ToString();
                    dt.Rows.Add(dr);
                }
                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells[1, 1].LoadFromDataTable(dt, true, TableStyles.None);
                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=BorrowDetail.xlsx");
                    excel.SaveAs(memoryStream); memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}