﻿using Service.Model;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NewsController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: Admin/News
        public ActionResult Index()
        {
            var news = db.News.Include(x => x.CategoryNew);
            return View(news.ToList());
        }


        public ActionResult GetNewsByCategory(int categoryId)
        {
            var news = db.News.Where(x => x.CategoryNewId == categoryId).ToList();
            return View("Index", news);
        }

        // GET: Admin/News/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            New @new = db.News.Find(id);
            if (@new == null)
            {
                return HttpNotFound();
            }
            return View(@new);
        }

        // GET: Admin/News/Create
        public ActionResult Create()
        {
            ViewBag.CategoryNewId = new SelectList(db.CategoryNews, "Id", "Name");
            return View();
        }

        // POST: Admin/News/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,PostId,Title,Short_Desciption,Post_Content,UrlSlug,Published,ViewCount,RateCount,TotalRate,Post_On,Modified,CategoryNewId")] New @new, HttpPostedFileBase postImage)
        {
            if (ModelState.IsValid)
            {
                if (postImage != null)
                {
                    @new.ImageUrl = UpLoadImage(postImage);
                    @new.ImageName = postImage.FileName;
                }
                db.News.Add(@new);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryNewId = new SelectList(db.CategoryNews, "Id", "Name", @new.CategoryNewId);
            return View(@new);
        }

        // GET: Admin/News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            New @new = db.News.Find(id);
            if (@new == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryNewId = new SelectList(db.CategoryNews, "Id", "Name", @new.CategoryNewId);
            return View(@new);
        }

        // POST: Admin/News/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,PostId,Title,Short_Desciption,Post_Content,UrlSlug,Published,ViewCount,RateCount,TotalRate,Post_On,Modified,CategoryNewId")] New @new, HttpPostedFileBase postImage)
        {
            if (ModelState.IsValid)
            {
                if (postImage != null && postImage.ContentLength > 0)
                {
                    @new.ImageUrl = UpLoadImage(postImage);
                    @new.ImageName = postImage.FileName;
                }
                db.Entry(@new).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryNewId = new SelectList(db.CategoryNews, "Id", "Name", @new.CategoryNewId);
            return View(@new);
        }

        // GET: Admin/News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            New @new = db.News.Find(id);
            if (@new == null)
            {
                return HttpNotFound();
            }
            return View(@new);
        }

        // POST: Admin/News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            New @new = db.News.Find(id);
            db.News.Remove(@new);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public string UpLoadImage(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string fileName = Path.GetFileName(file.FileName);
                string filePath = Path.Combine(Server.MapPath("~/Content/News"), fileName);
                file.SaveAs(filePath);
                return "/Content/News/" + fileName;
            }
            return null;
        }
    }
}