﻿using PagedList;
using Service.Model;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DevicesController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: Admin/Devices
        public ActionResult Index(int page = 1, int pageSize = 3)
        {
            var devices = db.Devices.Include(d => d.CategoryDevice).OrderBy(x => x.Id).ToPagedList(page, pageSize);
            return View(devices);
        }

        public ActionResult GetDeviceByCategory(string CategoryName, int page = 1, int pageSize = 3)
        {
            var listDevices = db.Devices.Where(x => x.CategoryDevice.Name.Contains(CategoryName)).OrderBy(x => x.Id).ToPagedList(page, pageSize);
            return View("Index", listDevices);
        }

        // GET: Admin/Devices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // GET: Admin/Devices/Create
        public ActionResult Create()
        {
            ViewBag.CategoryDeviceId = new SelectList(db.CategoryDevices, "Id", "Name");
            return View();
        }

        // POST: Admin/Devices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,TotalNumber,ImageName, ImageUrl,TotalBorrow,CompanyName,Date_Import,CategoryDeviceId")] Device device, HttpPostedFileBase postImage)
        {
            if (ModelState.IsValid)
            {
                if (postImage != null)
                {
                    device.ImageUrl = UpLoadImage(postImage);
                    device.ImageName = postImage.FileName;
                }
                device.TotalRest = device.TotalNumber;
                db.Devices.Add(device);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryDeviceId = new SelectList(db.CategoryDevices, "Id", "Name", device.CategoryDeviceId);
            return View(device);
        }

        // GET: Admin/Devices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryDeviceId = new SelectList(db.CategoryDevices, "Id", "Name", device.CategoryDeviceId);
            return View(device);
        }

        // POST: Admin/Devices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,TotalNumber,ImageName,ImageUrl,TotalBorrow,CompanyName,Date_Import,Modified,CategoryDeviceId")] Device device, HttpPostedFileBase postImage)
        {
            if (ModelState.IsValid)
            {
                if (postImage != null && postImage.ContentLength > 0)
                {
                    device.ImageUrl = UpLoadImage(postImage);
                    device.ImageName = postImage.FileName;
                }
                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryDeviceId = new SelectList(db.CategoryDevices, "Id", "Name", device.CategoryDeviceId);
            return View(device);
        }

        // GET: Admin/Devices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: Admin/Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = db.Devices.Find(id);
            db.Devices.Remove(device);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public string UpLoadImage(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string fileName = Path.GetFileName(file.FileName);
                string filePath = Path.Combine(Server.MapPath("~/Content/Image"), fileName);
                file.SaveAs(filePath);
                return "/Content/Image/" + fileName;
            }
            return null;
        }
    }
}