﻿using Service.Model;
using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IOT_Management.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TimelineEventsController : Controller
    {
        private ManagementLabContext db = new ManagementLabContext();

        // GET: Admin/TimelineEvents
        public ActionResult Index()
        {
            return View(db.TimelineEvents.ToList());
        }

        // GET: Admin/TimelineEvents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimelineEvent timelineEvent = db.TimelineEvents.Find(id);
            if (timelineEvent == null)
            {
                return HttpNotFound();
            }
            return View(timelineEvent);
        }

        // GET: Admin/TimelineEvents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/TimelineEvents/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create(int id, string description, string title, string start, string end)
        {
            if (ModelState.IsValid)
            {
                TimelineEvent timelineEvent = new TimelineEvent()
                {
                    Id = id,
                    Desciption = description,
                    Name = title,
                    Start_date = DateTime.Parse(start),
                    End_Date = DateTime.Parse(end)
                };
                db.TimelineEvents.Add(timelineEvent);
                db.SaveChanges();
                return Json(true);
            }

            return Json(false);
        }

        // GET: Admin/TimelineEvents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimelineEvent timelineEvent = db.TimelineEvents.Find(id);
            if (timelineEvent == null)
            {
                return HttpNotFound();
            }
            return View(timelineEvent);
        }

        // POST: Admin/TimelineEvents/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Edit(int id, string description, string title, string start, string end)
        {
            if (ModelState.IsValid)
            {
                TimelineEvent timelineEvent = db.TimelineEvents.Find(id);
                timelineEvent.Desciption = description;
                timelineEvent.Name = title;
                timelineEvent.Start_date = DateTime.Parse(start);
                timelineEvent.End_Date = DateTime.Parse(end);
                db.Entry<TimelineEvent>(timelineEvent).State = EntityState.Modified;
                db.SaveChanges();
                return Json(true);
            }
            return Json(false);
        }

        // GET: Admin/TimelineEvents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimelineEvent timelineEvent = db.TimelineEvents.Find(id);
            if (timelineEvent == null)
            {
                return HttpNotFound();
            }
            return View(timelineEvent);
        }

        // POST: Admin/TimelineEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        public JsonResult DeleteConfirmed(int id)
        {
            TimelineEvent timelineEvent = db.TimelineEvents.Find(id);
            db.TimelineEvents.Remove(timelineEvent);
            db.SaveChanges();
            return Json(true);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult FindAll()
        {
            var events = db.TimelineEvents.Select(x => new
            {
                id = x.Id,
                startDate = SqlFunctions.DatePart("yyyy", x.Start_date) + "-" + SqlFunctions.DatePart("MM", x.Start_date) +"-"+ SqlFunctions.DatePart("dd", x.Start_date),
                endDate = SqlFunctions.DatePart("year", x.End_Date) + "-" + SqlFunctions.DatePart("month", x.End_Date) + "-" + SqlFunctions.DatePart("day", x.End_Date),
                name = x.Name,
                description = x.Desciption
            }).ToArray();

            return Json(events, JsonRequestBehavior.AllowGet);

        }

        [NonAction]
        public string ConvertDateTime(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-mm-dd hh:mm tt");
        }
    }

}