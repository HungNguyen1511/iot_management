﻿using System.Web.Mvc;

namespace IOT_Management.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new {controller = "Device", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "IOT_Management.Areas.Admin.Controllers" }
            );
        }
    }
}