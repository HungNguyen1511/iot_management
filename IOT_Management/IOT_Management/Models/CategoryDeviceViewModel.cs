﻿namespace IOT_Management.Models
{
    public class CategoryDeviceViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string ImageUrl { set; get; }
    }
}