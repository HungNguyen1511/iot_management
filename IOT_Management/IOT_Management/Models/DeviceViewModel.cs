﻿using System;

namespace IOT_Management.Models
{
    public class DeviceViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int TotalNumber { set; get; }
        public int TotalBorrow { set; get; }
        public int TotalRest { set; get; }
        public string ImageUrl { set; get; }
        public string ImageName { set; get; }
        public string CompanyName { set; get; }
        public DateTime Date_Import { set; get; }
        public DateTime Modified { set; get; }
    }
}