﻿using System;

namespace IOT_Management.Models
{
    public class BorrowDetailViewModel
    {
        public int Id { set; get; }
        public int DeviceId { set; get; }
        public int UserId { set; get; }
        public bool IsReturn { set; get; }
        public DateTime Start_Date { set; get; }
        public DateTime End_Date { set; get; }
    }
}