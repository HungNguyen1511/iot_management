﻿namespace IOT_Management.Models
{
    public class CategoryNewViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Desciption { set; get; }
        public string ImageUrl { set; get; }

        public string ImageName { set; get; }
    }
}